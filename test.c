#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>

int main () {
    int servSockD = socket(AF_INET, SOCK_STREAM, 0);
  
    // string store data to send to client
    char serMsg[255] = "Message from the server to the "
                       "client \'Hello Client\' ";
  
    // define server address
    struct sockaddr_in servAddr;

    servAddr.sin_family = AF_INET;
    servAddr.sin_port = htons(9001);
    servAddr.sin_addr.s_addr = INADDR_ANY;
  
    // bind socket to the specified IP and port
    bind(servSockD, (struct sockaddr*)&servAddr,
         sizeof(servAddr));
  
    // listen for connections
    listen(servSockD, 1);
  
    // integer to hold client socket.
    int clientSocket = accept(servSockD, NULL, NULL);
  
    // send's messages to client socket
    send(clientSocket, serMsg, sizeof(serMsg), 0);

    close(servSockD);
  
    return 0;
}