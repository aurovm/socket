
case "$1" in
    #b | build )
    server | s )
        auro --dir dist server
        ;;
    client | c )
        auro --dir dist client
        ;;
    *)
        auro aulang socket.au dist/socket || break
        auro aulang -L dist server.au dist/server
        auro aulang -L dist client.au dist/client
        ;;
esac